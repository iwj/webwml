#use wml::debian::translation-check translation="907a0371eb05342911768c66ad56f028349d3301" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Mathy Vanhoef (NYUAD) og Eyal Ronen (Tel Aviv University &amp; KU Leuven) 
fandt adskillige sårbarheder i WPA-implementeringen i wpa_supplication 
(station) og hostapd (accesspoint).  Sårbarhederne er også samlet kendt som 
<q>Dragonblood</q>.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9495">CVE-2019-9495</a>

    <p>Cache-baseret sidekanalsangreb mod implementeringen af EAP-pwd:  En 
    angriber, der er i stand til at køre upriviligeret kode på målmaskinen 
    (herunder eksempelvis JavaScript-kode i en browser på en smartphone), 
    under handshake, kunne deducere tilstrækkeligt med oplysninger til at 
    finde adgangskoden i et ordbogsangreb.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9497">CVE-2019-9497</a>

    <p>Reflektionsangreb mod implementeringen af EAP-pwd server:  Manglende 
    validering af modtagne skalar- og elementværdier i 
    EAP-pwd-Commit-meddelelser, kunne medføre angreb, der var i stand til at 
    fuldføre autentifikationsudvekslingen uden at angriberen er nødt til at 
    kende adgangskoden.  Det medfører ikke at angriberen er i stand til at 
    udlede sessionsnøglen, fuldfuldføre den efterfølgende nøgleudveksling og 
    tilgå netværket.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9498">CVE-2019-9498</a>

    <p>EAP-pwd server manglende commit-validering af skalar/element: hostapd
    validerer ikke værdier modtaget i EAP-pwd-Commit-meddelelsen, så en angriber 
    kunne anvende en særligt fremstillet commitmeddelelse til at manipulere med 
    udvekslingen, for at få hostapd til at udlede en sessionsnøgle fra et 
    begrænset antal mulige værdier.  Det kunne medføre at angriberen var i stand 
    til at fuldføre autentifikationen og få adgang til netværket.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9499">CVE-2019-9499</a>

    <p>EAP-pwd peer manglende commitvalidering af skalar/element: 
    wpa_supplicant validerer ikke værdier modtaget i EAP-pwd-Commit-meddelelsen, 
    så en angriber kunne anvende en særligt fremstillet commitmeddelelse til at 
    manipulere med udvekslingen, for at få wpa_supplicant til at udlede en 
    sessionsnøgle fra et begrænset antal mulige værider.  Det kunne medføre at 
    angriberen blev i stand til at fuldføre autentifikationen og fungere som et 
    falskt accesspoint.</p>

</ul>

<p>Bemærk at tilnavnet Dragonblood også gælder 
<a href="https://security-tracker.debian.org/tracker/CVE-2019-9494">\
CVE-2019-9494</a> og 
<a href="https://security-tracker.debian.org/tracker/CVE-2014-9496">\
CVE-2014-9496</a>, hvilke er sårbarheder i SAE-protokollen i WPA3. SAE er ikke 
aktiveret i Debian stretch-opbygninger af wpa, hvilket dermed ikke som standard 
er sårbar.</p>

<p>På grund af kompleksiteten af tilbageførselsprocessen, er rettelsen af 
disse sårbarheder delvist.  Brugerne opfordres til at anvende stærke 
adgangskoder for at forhindre ordbogsangreb eller anvende en 2.7-baseret version 
fra stretch-backports (version større end 2:2.7+git20190128+0c1e29f-4).</p>

<p>I den stabile distribution (stretch), er disse problemer rettet i
version 2:2.4-1+deb9u3.</p>

<p>Vi anbefaler at du opgraderer dine wpa-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende wpa, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/wpa">\
https://security-tracker.debian.org/tracker/wpa</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4430.data"
