<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>The RTSP connection parser in the base GStreamer packages version 0.10,
which is a streaming media framework, was vulnerable against an
heap-based buffer overflow by sending a longer than allowed session id in
a response and including a semicolon to change the maximum length. This
could result in a remote code execution.</p>


<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
0.10.36-2+deb8u1.</p>

<p>We recommend that you upgrade your gst-plugins-base0.10 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1769.data"
# $Id: $
